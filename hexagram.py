## Hexagram Class

#TODO: return changing lines
#TODO: fetch data on individual changing lines
#TODO: read-back history file (colour/non-colour)
#TODO: bypass write-to-history

import sys
import gram_data
import colorama
from random import SystemRandom
from colorama import Fore, Back, Style



class Hexagram(object):
    """
    Hexagram object will generate a random sequence
    -from system random- unless one is passed in.

    The essential character of a Hexagram comes from it's sequence.

    Indeed as in the physical realm, all things are manifest in TIME.
    So too, the hexagram is revealed from the first line, starting
    at the bottom, so on up to the sixth line.

    Similarly, the most commonly accpeted progression of hexagrams
    from the first to the sixty-forth is The King Wen Sequence.
    """
    COLOR_GRAMS = {6 : Fore.MAGENTA + '▀▀▀▀   ▀▀▀▀' + Style.RESET_ALL,
                    7 : Fore.BLUE + '▀▀▀▀▀▀▀▀▀▀▀' + Style.RESET_ALL,
                    8 : Fore.BLUE + '▀▀▀▀   ▀▀▀▀' + Style.RESET_ALL,
                    9 : Fore.MAGENTA + '▀▀▀▀▀▀▀▀▀▀▀' + Style.RESET_ALL}

    GRAMS = {6 : '▀▀▀▀   ▀▀▀▀', 7 : '▀▀▀▀▀▀▀▀▀▀▀',
             8 : '▀▀▀▀   ▀▀▀▀', 9 : '▀▀▀▀▀▀▀▀▀▀▀'}

    def __init__(self, sequence=None):
        self._sequence = sequence if sequence else self._toss()

    def __repr__(self):
        return str(self.draw())
        #  return self.__str__()

    def __str__(self):
        return str(self._sequence)

    @property
    def sequence(self):
        """Consumes a list

        type: list is the Hexagram object's __repr__ type"""
        return self._sequence

    @property
    def dry_seq(self):
        dry = []    # base sequence without changing lines
        for line in self._sequence:
            if line == 6:
                dry.append(8)
            elif line == 9:
                dry.append(7)
            else:
               dry.append(line)
        return dry

    @property
    def is_changing(self) -> bool:
        """Does the hexagram contain any moving lines?"""
        if 6 in self._sequence or \
        9 in self._sequence:
            return True
        else:
            return False


    def _toss(self):
        """
        Emulates the coin method.

        This involves the tossing of THREE coins
        six times (one for each line of a hexagram)
        """
        seq = []
        for x in range(6):
            line = SystemRandom().randint(2,3) \
                    + SystemRandom().randint(2,3) \
                    + SystemRandom().randint(2,3)
            seq.append(line)
        return seq


    def next_gram(self):
        """
        Determine second gram derived from changing lines in
        given(1st) gram. Returned as a sequence
        """
        if not self.is_changing:
            return "There are no changing lines"
        else:
            next = []
            for line in self._sequence:
                if line == 6:
                    next.append(7)
                elif line == 9:
                    next.append(8)
                else:
                   next.append(line)
            return next


    def draw(self):
        """hexagrams are always drawn from the bottom-up"""
        colorama.init(autoreset=True)
        print()
        for line in reversed(self._sequence):
            print(str(line) + ' ' + Hexagram.GRAMS[line])



    def draw_return(self):
        """
        return a list of Lines for use in pretty print
        monochrome for now
        """
        x = []
        for line in reversed(self._sequence):
            x.append(str(line) + ' ' + Hexagram.COLOR_GRAMS[line])
        return x


    def fetch_data(self):
        """
        Pull data about a given hexagram from gram_data file
        Matching 'dry' sequence only
        """
        info = ['', '']
        for entry in gram_data.data:
            if self.dry_seq == entry['sequence']:
                num = gram_data.data.index(entry) + 1
                info.insert(-1, f'{Fore.YELLOW}No.{Style.RESET_ALL} {num}')
                for key in entry:
                    if key == 'sequence':
                        continue
                    info.insert(-1, f'{Fore.YELLOW}{key}:{Style.RESET_ALL} {entry[key]}')
        return info


    def pretty_print(self):
        """ Print gram + info to terminal in two columns """
        hex = self.draw_return()
        info = self.fetch_data()
        out = ""
        for a, b in zip(hex, info):
            out += '  '.join([a, b])
            out += '\n'
            #print(a + '  ' + b)
        return str(out)

