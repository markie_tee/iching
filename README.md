# I ching (for your terminal)

This is a simple module that provides a Hexagram class and can print a randomized result to the terminal. (one might have cron throw them a hexagram once a day)

The Hexagram Class initializes with a random sequence, unless passed in a specific hexagram.

*Changing* lines, will be printed in an alternate color. Color can be turned off. The final hexagram sequence is accessable via the object's `next_gram` property.

![example output](screens/screen_1.png)


For more information on the Book of Changes, go-to study in the west has been the [Richard Wilhelm translation](https://www.alibris.com/The-I-Ching-or-Book-of-Changes/book/40448388?matches=97) published by Princeton University 
\*with a forward by Carl Jung. 

The interpretations provided in this script come from [James DeKorn's](http://www.jamesdekorne.com/GBCh/GBCh.htm "The Gnostic Book Of Changes") great site that incorporates various modern translations.

----
## Examples
Print hexagram #1 <span style="color:skyblue">\[--testing\]</span> to stdout i.e. don't write to history file <span style="color:skyblue">\[--no-write\]</span> :
~~~python
$ ./ching.py -t -n
~~~
Print a specific hexagram \(#23\) with a changing line in space 4:
*Here we passed in a sequence to the <span style="color:skyblue">--gram</span> flag rather than the hexagram's number (in King Wen's progression*
~~~python
$ ./ching.py -g 877987

(...) Draw Hexagram output

$ tail -n8 hist.txt        # then I'll just tail the history file
~~~

----
### *TODO*:
  - print abridged commentary
  - txt only, image only, etc..
