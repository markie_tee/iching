# gram_data.py
# this is a dictionary of all 64 hexagrams

data = [{
    "sequence": [7, 7, 7, 7, 7, 7],
    "name": "Ch'ien ䷀",
    "alt-name": "The Creative",
    "link": "http://www.jamesdekorne.com/GBCh/hex1.htm"
  },{
    "sequence": [8, 8, 8, 8, 8, 8],
    "name": "K'un ䷁",
    "alt-name": "The Receptive",
    "link": "http://www.jamesdekorne.com/GBCh/hex2.htm"
  },{
    "sequence": [7, 8, 8, 8, 7, 8],
    "name": "Chun ䷂",
    "alt-name": "Difficulty at the Beginning",
    "link": "http://www.jamesdekorne.com/GBCh/hex3.htm"
  },{
    "sequence": [8, 7, 8, 8, 8, 7],
    "name": "Mêng ䷃",
    "alt-name": "Youthful Folly",
    "link": "http://www.jamesdekorne.com/GBCh/hex4.htm"
  },{
    "sequence": [7, 7, 7, 8, 7, 8],
    "name": "Hsü ䷄",
    "alt-name": "Waiting (Nourishment)",
    "link": "http://www.jamesdekorne.com/GBCh/hex5.htm"
  },{
    "sequence": [8, 7, 8, 7, 7, 7],
    "name": "Sung ䷅",
    "alt-name": "Conflict",
    "link": "http://www.jamesdekorne.com/GBCh/hex6.htm"
  },{
    "sequence": [8, 7, 8, 8, 8, 8],
    "name": "Shih ䷆",
    "alt-name": "The Army",
    "link": "http://www.jamesdekorne.com/GBCh/hex7.htm"
  },{
    "sequence": [8, 8, 8, 8, 7, 8],
    "name": "Pi ䷇",
    "alt-name": "Holding Together [Union]",
    "link": "http://www.jamesdekorne.com/GBCh/hex8.htm"
  },{
    "sequence": [7, 7, 7, 8, 7, 7],
    "name": "Hsiao Ch'u ䷈",
    "alt-name": "The Taming Power of The Small",
    "link": "http://www.jamesdekorne.com/GBCh/hex9.htm"
  },{
    "sequence": [7, 7, 8, 7, 7, 7],
    "name": "Lü ䷉",
    "alt-name": "Treading [Conduct]",
    "link": "http://www.jamesdekorne.com/GBCh/hex10.htm"
  },{
    "sequence": [7, 7, 7, 8, 8, 8],
    "name": "T'ai ䷊",
    "alt-name": "Peace",
    "link": "http://www.jamesdekorne.com/GBCh/hex11.htm"
  },{
    "sequence": [8, 8, 8, 7, 7, 7],
    "name": "P'i ䷋",
    "alt-name": "Standstill [Stagnation]",
    "link": "http://www.jamesdekorne.com/GBCh/hex12.htm"
  },{
    "sequence": [7, 8, 7, 7, 7, 7],
    "name": "T'ung Jên ䷌",
    "alt-name": "Fellowship With Men",
    "link": "http://www.jamesdekorne.com/GBCh/hex13.htm"
  },{
    "sequence": [7, 7, 7, 7, 8, 7],
    "name": "Ta Yu ䷍",
    "alt-name": "Posession in Great Measure",
    "link": "http://www.jamesdekorne.com/GBCh/hex14.htm"
  },{
    "sequence": [8, 8, 7, 8, 8, 8],
    "name": "Ch'ien ䷎",
    "alt-name": "Temperance",
    "link": "http://www.jamesdekorne.com/GBCh/hex15.htm"
  },{
    "sequence": [8, 8, 8, 7, 8, 8],
    "name": "Yü ䷏",
    "alt-name": "Enthusiasm",
    "link": "http://www.jamesdekorne.com/GBCh/hex16.htm"
  },{
    "sequence": [7, 8, 8, 7, 7, 8],
    "name": "Sui ䷐",
    "alt-name": "Following",
    "link": "http://www.jamesdekorne.com/GBCh/hex17.htm"
  },{
    "sequence": [8, 7, 7, 8, 8, 7],
    "name": "Ku ䷑",
    "alt-name": "Work on What Has Been Spoiled [Decay]",
    "link": "http://www.jamesdekorne.com/GBCh/hex18.htm"
  },{
    "sequence": [7, 7, 8, 8, 8, 8],
    "name": "Lin ䷒",
    "alt-name": "Approach",
    "link": "http://www.jamesdekorne.com/GBCh/hex19.htm"
  },{
    "sequence": [8, 8, 8, 8, 7, 7],
    "name": "Kuan ䷓",
    "alt-name": "Contemplation (View)",
    "link": "http://www.jamesdekorne.com/GBCh/hex20.htm"
  },{
    "sequence": [7, 8, 8, 7, 8, 7],
    "name": "Shih Ho ䷔",
    "alt-name": "Biting Through",
    "link": "http://www.jamesdekorne.com/GBCh/hex21.htm"
  },{
    "sequence": [7, 8, 7, 8, 8, 7],
    "name": "Pi ䷕",
    "alt-name": "Grace",
    "link": "http://www.jamesdekorne.com/GBCh/hex22.htm"
  },{
    "sequence": [8, 8, 8, 8, 8, 7],
    "name": "Po ䷖",
    "alt-name": "Spliting Apart",
    "link": "http://www.jamesdekorne.com/GBCh/hex23.htm"
  },{
    "sequence": [7, 8, 8, 8, 8, 8],
    "name": "Fu ䷗",
    "alt-name": "Return (The Turning Poin)",
    "link": "http://www.jamesdekorne.com/GBCh/hex24.htm"
  },{
    "sequence": [7, 8, 8, 7, 7, 7],
    "name": "Wu Wang ䷘",
    "alt-name": "Innocence (The Unexpected)",
    "link": "http://www.jamesdekorne.com/GBCh/hex25.htm"
  },{
    "sequence": [7, 7, 7, 8, 8, 7],
    "name": "Ta Ch'u ䷙",
    "alt-name": "The Taming Power of the Great",
    "link": "http://www.jamesdekorne.com/GBCh/hex26.htm"
  },{
    "sequence": [7, 8, 8, 8, 8, 7],
    "name": "I ䷚",
    "alt-name": "The Corners of the Mouth (Providing Nourishment)",
    "link": "http://www.jamesdekorne.com/GBCh/hex27.htm"
  },{
    "sequence": [8, 7, 7, 7, 7, 8],
    "name": "Ta Kuo ䷛",
    "alt-name": "Preponderance of the Great",
    "link": "http://www.jamesdekorne.com/GBCh/hex28.htm"
  },{
    "sequence": [8, 7, 8, 8, 7, 8],
    "name": "K'an ䷜",
    "alt-name": "The Abysmal / Danger (Water)",
    "link": "http://www.jamesdekorne.com/GBCh/hex29.htm"
  },{
    "sequence": [7, 8, 7, 7, 8, 7],
    "name": "Li ䷝",
    "alt-name": "The Clinging, Fire",
    "link": "http://www.jamesdekorne.com/GBCh/hex30.htm"
  },{
    "sequence": [8, 8, 7, 7, 7, 8],
    "name": "Hsien ䷞",
    "alt-name": "Influence (Wooing)",
    "link": "http://www.jamesdekorne.com/GBCh/hex31.htm"
  },{
    "sequence": [8, 7, 7, 7, 8, 8],
    "name": "Hêng ䷟",
    "alt-name": "Duration",
    "link": "http://www.jamesdekorne.com/GBCh/hex32.htm"
  },{
    "sequence": [8, 8, 7, 7, 7, 7],
    "name": "Tun ䷠",
    "alt-name": "Retreat",
    "link": "http://www.jamesdekorne.com/GBCh/hex33.htm"
  },{
    "sequence": [7, 7, 7, 7, 8, 8],
    "name": "Ta Chuang ䷡",
    "alt-name": "The Power of the Great",
    "link": "http://www.jamesdekorne.com/GBCh/hex34.htm"
  },{
    "sequence": [8, 8, 8, 7, 8, 7],
    "name": "Chin ䷢",
    "alt-name": "Progress",
    "link": "http://www.jamesdekorne.com/GBCh/hex35.htm"
  },{
    "sequence": [7, 8, 7, 8, 8, 8],
    "name": "Ming I ䷣",
    "alt-name": "Darkening of the Light",
    "link": "http://www.jamesdekorne.com/GBCh/hex36.htm"
  },{
    "sequence": [7, 8, 7, 8, 7, 7],
    "name": "Chia Jên ䷤",
    "alt-name": "The Family [The Clan]",
    "link": "http://www.jamesdekorne.com/GBCh/hex37.htm"
  },{
    "sequence": [7, 7, 8, 7, 8, 7],
    "name": "K'uei ䷥",
    "alt-name": "Opposition",
    "link": "http://www.jamesdekorne.com/GBCh/hex38.htm"
  },{
    "sequence": [8, 8, 7, 8, 7, 8],
    "name": "Chien ䷦",
    "alt-name": "Obstruction",
    "link": "http://www.jamesdekorne.com/GBCh/hex39.htm"
  },{
    "sequence": [8, 7, 8, 7, 8, 8],
    "name": "Hsieh ䷧",
    "alt-name": "Deliverance",
    "link": "http://www.jamesdekorne.com/GBCh/hex40.htm"
  },{
    "sequence": [7, 7, 8, 8, 8, 7],
    "name": "sun ䷨",
    "alt-name": "Decrease",
    "link": "http://www.jamesdekorne.com/GBCh/hex41.htm"
  },{
    "sequence": [7, 8, 8, 8, 7, 7],
    "name": "I ䷩",
    "alt-name": "Increase",
    "link": "http://www.jamesdekorne.com/GBCh/hex42.htm"
  },{
    "sequence": [7, 7, 7, 7, 7, 8],
    "name": "Kuai ䷪",
    "alt-name": "Break-through (Resoluteness)",
    "link": "http://www.jamesdekorne.com/GBCh/hex43.htm"
  },{
    "sequence": [8, 7, 7, 7, 7, 7],
    "name": "Kou ䷫",
    "alt-name": "Coming to Meet",
    "link": "http://www.jamesdekorne.com/GBCh/hex44.htm"
  },{
    "sequence": [8, 8, 8, 7, 7, 8],
    "name": "Ts'ui ䷬",
    "alt-name": "Gathering Together [Massing]",
    "link": "http://www.jamesdekorne.com/GBCh/hex45.htm"
  },{
    "sequence": [8, 7, 7, 8, 8, 8],
    "name": "Shêng ䷭",
    "alt-name": "Pushing Upward",
    "link": "http://www.jamesdekorne.com/GBCh/hex46.htm"
  },{
    "sequence": [8, 7, 8, 7, 7, 8],
    "name": "K'un ䷮",
    "alt-name": "Oppression (Exhaustion)",
    "link": "http://www.jamesdekorne.com/GBCh/hex47.htm"
  },{
    "sequence": [8, 7, 7, 8, 7, 8],
    "name": "Ching ䷯",
    "alt-name": "The Well",
    "link": "http://www.jamesdekorne.com/GBCh/hex48.htm"
  },{
    "sequence": [7, 8, 7, 7, 7, 8],
    "name": "Ko ䷯",
    "alt-name": "Revolution (Molting)",
    "link": "http://www.jamesdekorne.com/GBCh/hex49.htm"
  },{
    "sequence": [8, 7, 7, 7, 8, 7],
    "name": "Ting ䷱",
    "alt-name": "The Cauldron",
    "link": "http://www.jamesdekorne.com/GBCh/hex50.htm"
  },{
    "sequence": [7, 8, 8, 7, 8, 8],
    "name": "Chên ䷲",
    "alt-name": "The Arousing (Shock, Thunder)",
    "link": "http://www.jamesdekorne.com/GBCh/hex51.htm"
  },{
    "sequence": [8, 8, 7, 8, 8, 7],
    "name": "Kên ䷳",
    "alt-name": "Keeping Still, Mountain",
    "link": "http://www.jamesdekorne.com/GBCh/hex52.htm"
  },{
    "sequence": [8, 8, 7, 8, 7, 7],
    "name": "Chien ䷴",
    "alt-name": "Developement (Gradual Progress)",
    "link": "http://www.jamesdekorne.com/GBCh/hex53.htm"
  },{
    "sequence": [7, 7, 8, 7, 8, 8],
    "name": "Kuei Mei ䷵",
    "alt-name": "Propriety/Making-Do",
    "link": "http://www.jamesdekorne.com/GBCh/hex54.htm"
  },{
    "sequence": [7, 8, 7, 7, 8, 8],
    "name": "Fêng ䷶",
    "alt-name": "Abundance [Fullness]",
    "link": "http://www.jamesdekorne.com/GBCh/hex55.htm"
  },{
    "sequence": [8, 8, 7, 7, 8, 7],
    "name": "Lü ䷷",
    "alt-name": "The Wanderer",
    "link": "http://www.jamesdekorne.com/GBCh/hex56.htm"
  },{
    "sequence": [8, 7, 7, 8, 7, 7],
    "name": "Sun ䷸",
    "alt-name": "The Gentle (The Penetrating, Wind)",
    "link": "http://www.jamesdekorne.com/GBCh/hex57.htm"
  },{
    "sequence": [7, 7, 8, 7, 7, 8],
    "name": "Tui ䷹",
    "alt-name": "The Joyous, Lake",
    "link": "http://www.jamesdekorne.com/GBCh/hex58.htm"
  },{
    "sequence": [8, 7, 8, 8, 7, 7],
    "name": "Huan ䷺",
    "alt-name": "Dispersion [Dissolution]",
    "link": "http://www.jamesdekorne.com/GBCh/hex59.htm"
  },{
    "sequence": [7, 7, 8, 8, 7, 8],
    "name": "Chieh ䷻",
    "alt-name": "Limitation",
    "link": "http://www.jamesdekorne.com/GBCh/hex60.htm"
  },{
    "sequence": [7, 7, 8, 8, 7, 7],
    "name": "Chung Fu ䷼",
    "alt-name": "Inner Truth",
    "link": "http://www.jamesdekorne.com/GBCh/hex61.htm"
  },{
    "sequence": [8, 8, 7, 7, 8, 8],
    "name": "Hsiao Kuo ䷽",
    "alt-name": "Preponderance of the Small",
    "link": "http://www.jamesdekorne.com/GBCh/hex62.htm"
  },{
    "sequence": [7, 8, 7, 8, 7, 8],
    "name": "Chi Chi ䷾",
    "alt-name": "After Completion",
    "link": "http://www.jamesdekorne.com/GBCh/hex63.htm"
  },{
    "sequence": [8, 7, 8, 7, 8, 7],
    "name": "Wei Chi ䷿",
    "alt-name": "Before Completion",
    "link": "http://www.jamesdekorne.com/GBCh/hex64.htm"
  }
]
