#!/usr/bin/python

from pathlib import Path
from argparse import ArgumentParser
from datetime import datetime
from hexagram import Hexagram



# Construct arg-parser
# current: gram, testing, changing, no-write, question
parser = ArgumentParser(description="toss the coins + generate hexagram")

parser.add_argument( '-g', '--gram',
                    action='store',
                    help="You can input a sequence, or a number (from King Wen's sequence)"
)

parser.add_argument( '-t', '--testing',
                    action='store_true',
                    help="writes to an alternate hist file"
)

parser.add_argument( '-c', '--changing',
                    action='store_true',
                    help="Set to show NO Changing lines in output"
)

parser.add_argument( '-n', '--no-write',
                    dest='nowrite',
                    action='store_true',
                    help="write directly to STDOUT, skipping history file"
)

parser.add_argument( '-q', '--question',
                    action='store',
                    help="question to the output. just for the hex of it.."
)
args = parser.parse_args()


def toss(args):

    # Instantiate our Hexagram Class depending on [non]input
    if args.gram:
        from gram_data import data
        if len(args.gram) <= 2:
            dataz = data[int(args.gram) - 1]['sequence']
            graham = Hexagram(dataz)
        elif len(args.gram) == 6:
            dataz = [int(i) for i in list(args.gram)]    # convert our string into list of int's
            graham = Hexagram(dataz)
        elif 2 < len(args.gram) > 6:
            graham = input("re-enter the intended hexagram:") 
    else:
        graham = Hexagram()

    if args.testing:
        history = Path('hist-test.txt')
        if not args.gram:
            graham = Hexagram([7,7,7,7,7,7])
    else:
        history = Path('hist.txt')

    if args.changing:
        dry = graham.dry_seq
        graham = Hexagram(list(dry))


    now_result = graham.pretty_print()
    time_stamp = datetime.now()
    datetime_out = str( "\n\t       " + time_stamp.strftime( "%b-%d-%Y %H:%M" ) )

    if args.question:
        now_result += "question: " + str(args.question)

    if args.nowrite:
        print( datetime_out + "\n" + now_result + "\n")
    else:
        with history.open("a") as f:
            f.write( datetime_out + "\n" + now_result )
        print(f'written to {history}')




if __name__ == "__main__":
    print('The oracle uses ambiguity to develop your intuition..')
    toss(args)
    if args.testing:
        print(args)

