"""mirror.py is a bunch of 'methods' that we use to probe a Hexagram object"""


def next_gram(self):
    """
    Determine second gram derived from changing lines in
    given(1st) gram. Returned as a sequence
    """
    if not self.is_changing:
        return "There are no changing lines"
    else:
        next = []
        for line in self._sequence:
            if line == 6:
                next.append(7)
            elif line == 9:
                next.append(8)
            else:
               next.append(line)
        return next


def draw_return(self):
    """
    return a list of Lines for use in pretty print
    monochrome for now
    """
    x = []
    for line in reversed(self._sequence):
        x.append(str(line) + ' ' + Hexagram.COLOR_GRAMS[line])
    return x


def fetch_data(self):
    """
    Pull data about a given hexagram from gram_data file
    Matching 'dry' sequence only
    """
    info = ['', '']
    for entry in gram_data.data:
        if self.dry_seq == entry['sequence']:
            num = gram_data.data.index(entry) + 1
            info.insert(-1, f'{Fore.YELLOW}No.{Style.RESET_ALL} {num}')
            for key in entry:
                if key == 'sequence':
                    continue
                info.insert(-1, f'{Fore.YELLOW}{key}:{Style.RESET_ALL} {entry[key]}')
    return info


def pretty_print(self):
    """ Print gram + info to terminal in two columns """
    hex = self.draw_return()
    info = self.fetch_data()
    out = ""
    for a, b in zip(hex, info):
        out += '  '.join([a, b])
        out += '\n'
        #print(a + '  ' + b)
    return str(out)
